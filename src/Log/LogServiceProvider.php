<?php

namespace Container\Log;

use Illuminate\Support\ServiceProvider;

/**
 * ログ サービスプロバイダー
 *
 * @author tatsuki.kubota
 */
class LogServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('ExtensionLog', 'Container\Log\ExtensionLog');
    }
}
