<?php

namespace Container\Validation;

use Container\Support\Facades\DB;
use Illuminate\Validation\PresenceVerifierInterface;

/**
 * Validate Database Presence クラス
 *
 * @author tatsuki.kubota
 */
class DatabasePresenceVerifier implements PresenceVerifierInterface
{
    /**
     * Count the number of objects in a collection having the given value.
     *
     * @param  string  $collection
     * @param  string  $column
     * @param  string  $value
     * @param  int     $excludeId
     * @param  string  $idColumn
     * @param  array   $extra
     * @return int
     */
    public function getCount($collection, $column, $value, $excludeId = null, $idColumn = null, array $extra = [])
    {
        $query   = "SELECT COUNT(*) AS count FROM $collection WHERE $column = :$column";
        $prepare = [':' . $column => $value];

        if (!is_null($excludeId) && $excludeId != 'NULL') {
            $idColumn = $idColumn ? $idColumn : 'id';
            $query    .= "AND $idColumn <> :id";
            $prepare  += [':' . $idColumn => $excludeId];
        }

        $this->addConditions($query, $prepare, $extra);

        return DB::select($query, $prepare)[0]['count'];
    }

    /**
     * Count the number of objects in a collection with the given values.
     *
     * @param  string  $collection
     * @param  string  $column
     * @param  array   $values
     * @param  array   $extra
     * @return int
     */
    public function getMultiCount($collection, $column, array $values, array $extra = [])
    {
        $query = "SELECT COUNT(*) AS count FROM $collection WHERE $column = :$column";
        $prepare = [':' . $column => $values];

        $this->addConditions($query, $prepare, $extra);

        return DB::select($query, $prepare)[0]['count'];
    }

    /**
     * Add the given conditions to the query.
     *
     * @param  string  $query
     * @param  array   $prepare
     * @return void
     */
    protected function addConditions(&$query, &$prepare, $conditions)
    {
        foreach ($conditions as $key => $value) {
            $this->addWhere($query, $prepare, $key, $value);
        }
        return $query;
    }

    /**
     * Add a "where" clause to the given query.
     *
     * @param  string  $query
     * @param  array   $prepare
     * @param  string  $key
     * @param  string  $extraValue
     * @return void
     */
    protected function addWhere(&$query, &$prepare, $key, $extraValue)
    {
        if ($extraValue === 'NULL') {
            $query .= " AND $key IS NULL";
        } elseif ($extraValue === 'NOT_NULL') {
            $query .= " AND $key IS NOT NULL";
        } elseif (Str::startsWith($extraValue, '!')) {
            $query   .= " AND $key != :$key";
            $prepare = [':' . $key => mb_substr($extraValue, 1)];
        } else {
            $query .= " AND $key = :$key";
            $prepare = [':' . $key => $extraValue];
        }
    }

    /**
     * Set the connection to be used.
     *
     * @param  string  $connection
     * @return void
     */
    public function setConnection($connection)
    {
        if (!empty($connection)) {
            DB::connect($connection);
        }
    }
}
