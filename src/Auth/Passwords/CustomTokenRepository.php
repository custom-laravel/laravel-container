<?php

namespace Container\Auth\Passwords;

use Carbon\Carbon;
use Illuminate\Auth\Passwords\TokenRepositoryInterface;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Str;

/**
 * パスワードリセットトークン管理クラス
 *
 * @author tatsuki.kubota
 */
class CustomTokenRepository implements TokenRepositoryInterface
{
    /**
     * The Hasher implementation.
     *
     * @var \Illuminate\Contracts\Hashing\Hasher
     */
    protected $hasher;

    /**
     * The hashing key.
     *
     * @var string
     */
    protected $hashKey;

    /**
     * The number of seconds a token should last.
     *
     * @var int
     */
    protected $expires;

    /**
     * Data Access Object
     *
     * @var \Container\Support\Dao\PasswordResets
     */
    private $dao;

    /**
     * Create a new token repository instance.
     *
     * @param  \Illuminate\Database\ConnectionInterface  $connection
     * @param  \Illuminate\Contracts\Hashing\Hasher  $hasher
     * @param  string  $table
     * @param  string  $hashKey
     * @param  string  $dao
     * @param  int  $expires
     * @return void
     */
    public function __construct(
        HasherContract $hasher,
        $hashKey,
        $dao,
        $expires = 60)
    {
        $this->hasher     = $hasher;
        $this->hashKey    = $hashKey;
        $this->expires    = $expires * 60;
        $this->dao        = new $dao;
    }

    /**
     * Create a new token record.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @return string
     */
    public function create(CanResetPasswordContract $user)
    {
        $email = $user->getEmailForPasswordReset();

        $this->dao->deleteExisting($email);

        // We will create a new, random token for the user so that we can e-mail them
        // a safe link to the password reset form. Then we will insert a record in
        // the database so that we can verify the token within the actual reset.

        // create new token
        $token = hash_hmac('sha256', Str::random(40), $this->hashKey);

        $this->dao->insertResetToken($email, $this->hasher->make($token), new Carbon);

        return $token;
    }

    /**
     * Determine if a token record exists and is valid.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $token
     * @return bool
     */
    public function exists(CanResetPasswordContract $user, $token)
    {
        $record = $this->dao->selectEmail($user->getEmailForPasswordReset());

        return isset($record[0]) &&
            !Carbon::parse($record[0]['created_at'])->addSeconds($this->expires)->isPast() &&
            $this->hasher->check($token, $record[0]['token']);
    }

    /**
     * Delete a token record by user.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @return void
     */
    public function delete(CanResetPasswordContract $user)
    {
        $this->dao->deleteExisting($user->getEmailForPasswordReset());
    }

    /**
     * Delete expired tokens.
     *
     * @return void
     */
    public function deleteExpired()
    {
        $expiredAt = Carbon::now()->subSeconds($this->expires);

        $this->dao->deleteExpired($expiredAt);
    }
}
